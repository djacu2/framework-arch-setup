# install network manager
pacman --noconfirm -S networkmanager

# setup network manager
systemctl enable systemd-resolved.service
systemctl enable NetworkManager.service