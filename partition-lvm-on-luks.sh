#!/usr/bin/bash

# causes the shell to exit if any subcommand or pipeline returns a non-zero status
# possibly dangerous or lazy
set -e

# define a disk and set partition sizes
disk='nvme0n1'
init_mibs='1'
boot_mibs='256'
swap_mibs=$(free -m | awk '/^Mem:/{print $2}')
root_mibs='128000'
var_mibs='64000'
tmp_mibs='8000'

# delete all partitions and overwrite disk with random data
disk_dev="/dev/${disk}"
shred --random-source=/dev/urandom --iterations=1 "${disk_dev}"

# calculate boot partition sizes.
boot_from="${init_mibs}"
swap_from="$(expr ${boot_from} + ${boot_mibs})"
# parted end position's are inclusive.
boot_to="$(expr ${swap_from} - 1)"

# create a new disklabel
parted -s "${disk_dev}" mklabel gpt

# create and format boot partition
parted -s "${disk_dev}" mkpart boot fat32 "${boot_from}MiB" "${boot_to}MiB"
parted -s "${disk_dev}" set 1 esp on
disk_part1="${disk}p1"
mkfs.fat -F 32 "/dev/${disk_part1}"

# create partition for disk encryption
parted -s "${disk_dev}" mkpart cryptlvm "${swap_from}MiB" '100%'
disk_part2="${disk}p2"
cryptsetup luksFormat "/dev/${disk_part2}"
cryptsetup open "/dev/${disk_part2}" cryptlvm
pvcreate /dev/mapper/cryptlvm
volume_group="volgrp"
vgcreate "${volume_group}" /dev/mapper/cryptlvm

# create the lv partitions.
lvcreate -L "${swap_mibs}M" "${volume_group}" -n swap
lvcreate -L "${root_mibs}M" "${volume_group}" -n root
lvcreate -L "${var_mibs}M" "${volume_group}" -n var
lvcreate -L "${tmp_mibs}M" "${volume_group}" -n tmp
# leaving some free space in the VG so an LV can be expanded easily without unmounting
lvcreate -l '90%FREE' "${volume_group}" -n home

# format the lv partitions
mkswap "/dev/${volume_group}/swap"
mkfs.ext4 "/dev/${volume_group}/root"
mkfs.ext4 "/dev/${volume_group}/var"
mkfs.ext4 "/dev/${volume_group}/tmp"
mkfs.ext4 "/dev/${volume_group}/home"

# mount all partitions
mount "/dev/${volume_group}/root" /mnt
mkdir /mnt/boot
mount "/dev/${disk_part1}" /mnt/boot
swapon "/dev/${volume_group}/swap"
mkdir /mnt/var
mount "/dev/${volume_group}/var" /mnt/var
mkdir /mnt/tmp
mount "/dev/${volume_group}/tmp" /mnt/tmp
mkdir /mnt/home
mount "/dev/${volume_group}/home" /mnt/home

# install the base system and generate an fstab file
pacstrap /mnt base linux linux-firmware lvm2 vim iwd intel-ucode man-db man-pages texinfo
genfstab -U /mnt >> /mnt/etc/fstab

echo "Perform the following tasks manually"
echo "- chroot into the system:  arch-chroot /mnt"
echo "- set an admin password:   passwd"
echo "- then continue with the post-chroot.sh script"