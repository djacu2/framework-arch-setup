#!/usr/bin/bash

# causes the shell to exit if any subcommand or pipeline returns a non-zero status
# possibly dangerous or lazy
set -e

# set options
lang='en_US.UTF-8'
locale='en_US.UTF-8 UTF-8'
keymap='us'
hostname='adalon'
ucode='intel-ucode'

# additional packages
pacman --noconfirm -S sudo openvpn screen less fish

# localization
ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
timedatectl set-ntp true
hwclock --systohc
echo "${locale}" >> /etc/locale.gen
locale-gen
echo "LANG=${lang}" >> /etc/locale.conf
echo "KEYMAP=${keymap}" >> /etc/vconsole.conf

# network configuration
echo -n "${hostname}" > /etc/hostname
echo "127.0.0.1   localhost" >> /etc/hosts
echo "::1         localhost" >> /etc/hosts
echo "127.0.1.1   ${hostname}.localdomain ${hostname}" >> /etc/hosts

# systemd boot loader
systemd-machine-id-setup
bootctl --path=/boot install

uuid=$(blkid --match-tag UUID -o value /dev/nvme0n1p2)
cat <<EOF >/boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /${ucode}.img
initrd  /initramfs-linux.img
options cryptdevice=UUID=${uuid}:cryptlvm root=/dev/volgrp/root resume=/dev/volgrp/swap
EOF

cat <<EOF >/boot/loader/loader.conf
default arch
timeout 0
editor  0
EOF

# update initramfs
new_hooks="HOOKS=(base udev autodetect keyboard keymap modconf block encrypt lvm2 filesystems resume fsck)"
sed -i "s/^HOOKS=.*/${new_hooks}/" /etc/mkinitcpio.conf
mkinitcpio -P

# install a network manager for internet connections
./networkmanager.sh

# add a new non-root user
./add-user.sh

echo "Perform the following tasks manually:"
echo "- exit the environment: exit"
echo "- unmount recursively:  umount -R /mnt"
echo "- restart the system:   shutdown -h now"
echo "- remove the USB media and start the system"