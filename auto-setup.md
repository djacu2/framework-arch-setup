# Install

Set the keyboard layout explicitly:

    # loadkeys us

Connect to a WiFi network:

    # iwctl --passphrase <phrase> station <device> connect <ssid>

Pick a device:

    # lsblk

Download the scripts:

    # TODO

Modify the script according to your environment and needs, and run it:

    # chmod +x partition-lvm-on-luks.sh
    # chmod +x post-chroot.sh

Run the partition script and follow the instructions printed by the script:

    # ./partition-lvm-on-luks.sh

Run the setup script and follow the instructions printed by the script:

    # ./post-chroot.sh

After reboot, if connecting to WiFi us nmcli to connect as follows:

    # nmcli device wifi connect <SSID> password <password>
