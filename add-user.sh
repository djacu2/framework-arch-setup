#!/usr/bin/bash

# add the new user to the wheel group and ask for a password
echo "Enter a new username:"
read new_user
useradd --create-home --groups wheel "${new_user}"
echo "Enter ${new_user}'s password:"
passwd ${new_user}

# add wheel to the list of sudoers
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
